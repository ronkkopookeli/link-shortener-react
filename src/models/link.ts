export interface Link {
    url: string,
    short: string,
    long: string
}

export interface ShortLink {
    url: string,
    short: string
}