import { useEffect } from "react";
import ApiService from "../services/api.service"

type RedirectProps = {
    urlParam: string,
    linkFetched?: boolean
};

const Redirect = (props: RedirectProps) => {
    const { urlParam, linkFetched } = props;
    useEffect(() => {
        if(urlParam && urlParam !== '/'){
            ApiService.getLinkByShort(urlParam)
            .then((res: any) => {
                if (res){
                    window.location.href = res.data.url;
                } else {
                    console.log('fail');
                }
            })
            .catch((e: Error) => {
                console.error(e);
            });
        }
    }, []);
    return (
        <div className="submit-form">
    {linkFetched ? (
        <div>
        Loading
        </div>
    ) : (
        <div>
        Welcome to link shortener!
        </div>
    )}
    </div>
    );
};

export default Redirect;
