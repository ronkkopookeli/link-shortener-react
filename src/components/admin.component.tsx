import { useEffect, useState } from "react";
import apiService from "../services/api.service";

type AdminProps = {
    urlParam: string
};

const Admin = (props: AdminProps) => {
    const { urlParam } = props;
    const [ count, setCount ] = useState(0);
    const [ deleted, setDeleted ] = useState(false);
    const long = urlParam && urlParam.split('/admin/')[1];
    const deleteLink = () => {
        if(long) {
            apiService.deleteLink(long)
            .then((res: any) => {
                if(res.status == 204) {
                    setDeleted(true);
                } else {
                    console.log(res);
                }
            })
            .catch((e: Error) => {
                console.error(e);
            });
        }
        
    }

    useEffect(() => {
        if(long){
            apiService.getClicksByLong(long)
            .then((res: any) => {
                if (res){
                    setCount(res.data.count);
                } else {
                    console.log('fail');
                }
            })
            .catch((e: Error) => {
                console.error(e);
            });
        }
    }, []);
    return (
        <div className="submit-form">
            {!deleted ?
            <div>
                <div>
                    <p>Counts clicked last 24h: {count}</p>
                </div>
                <button onClick={deleteLink} className="btn btn-warning">
                    Delete link
                </button>
            </div>
            :
            <div>
            Link deleted
            </div>
            }
        </div>
    );
};

export default Admin;