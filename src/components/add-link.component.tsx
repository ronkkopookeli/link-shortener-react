import { Component, ChangeEvent } from "react";
import { Link } from "../models/link";
import ApiService from "../services/api.service"

type Props = Link & {
    submitted: boolean
};

type State = {};

export default class AddLink extends Component<State, Props> {
    render() {
        const {url, submitted, short, long} = this.state;
        return (
            <div className="submit-form">
        {submitted ? (
          <div>
            <h4>Shortened URL:</h4>
            <h5>{short}</h5>
            <h4>Long URL:</h4>
            <h5>{long}</h5>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">Original URL</label>
              <input
                type="text"
                className="form-control"
                id="title"
                required
                value={url}
                onChange={this.onChangeLink}
                name="title"
              />
            </div>

            <button onClick={this.saveLink} className="btn btn-success">
            Shorten!
            </button>
          </div>
        )}
      </div>
        );
    };

    constructor(
        props: Props
    ) {
        super(props);
        this.onChangeLink = this.onChangeLink.bind(this);
        this.saveLink = this.saveLink.bind(this);
        

        this.state = {
            url: '',
            short: '',
            long: '',
            submitted: false
        };
    };

    onChangeLink(e: ChangeEvent<HTMLInputElement>) {
        this.setState({
            url: e.target.value
        });
    };

    saveLink() {
        ApiService.createLink(this.state.url)
            .then((res: any) => {
                this.setState({
                    url: res.data.url,
                    short: res.data.short,
                    long: res.data.long,
                    submitted: true
                });
            })
            .catch((e: Error) => {
                console.error(e);
            });
    };
}