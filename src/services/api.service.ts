import httpCommon from "../http-common";
import { Link, ShortLink } from "../models/link";

class ApiService {
    createLink(data: string) {
        const jsonData = {
            url: data
        }
        return httpCommon.post<Link>('/links/', jsonData);
    }

    getLinkByShort(short: string) {
        return httpCommon.get<ShortLink>(`/links${short}`);
    }

    deleteLink(long: string) {
        return httpCommon.delete<boolean>(`/links/${long}`);
    }

    getClicksByLong(long: string) {
        return httpCommon.get<number>(`/clicks/${long}`);
    }
}

export default new ApiService();