import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Routes, Route, useLocation } from 'react-router-dom';
import AddLink from './components/add-link.component';
import Redirect from './components/redirect.component';
import Admin from './components/admin.component';
import { CLIENT_RENEG_WINDOW } from 'tls';

const App = () => {
  const location = useLocation();

  console.log(location.pathname);
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to={"/"} className="navbar-brand">
          Shortener
        </Link>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/add"} className="nav-link">
              Add new
            </Link>
          </li>
        </div>
      </nav>

      <div className="container mt-3">
        <Routes>
          <Route path="/add" element={<AddLink />} />
          <Route path={`${location.pathname}`} element={
          location.pathname.startsWith('/admin/') ?
            <Admin urlParam={location.pathname} /> :
            <Redirect urlParam={location.pathname} />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
